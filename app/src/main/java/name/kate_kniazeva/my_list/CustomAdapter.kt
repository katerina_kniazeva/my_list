package name.kate_kniazeva.my_list

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView

class CustomAdapter(
    private val mList: List<ItemsViewModel>) : RecyclerView.Adapter<CustomAdapter.ViewHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_view_design, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, @SuppressLint("RecyclerView") position: Int) {

        val ItemsViewModel = mList[position]


        holder.imageView.setImageResource(ItemsViewModel.image)

        holder.textView1.text = ItemsViewModel.text1
        holder.textView2.text = ItemsViewModel.text2
        holder.imageView.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                val activity = v!!.context as AppCompatActivity
                val itemImageView = v?.findViewById<ImageView>(R.id.imageview)
                if (itemImageView != null) {
                    ViewCompat.setTransitionName(itemImageView, "item_image")
                }
                val secondFragment = NextFragment()

                activity.supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment1_layout, NextFragment())
                    .addToBackStack(null)
                    .commit()
            }
        }
        )


    }


    override fun getItemCount(): Int {
        return mList.size
    }


    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val imageView: ImageView = itemView.findViewById(R.id.imageview)
        val textView1: TextView = itemView.findViewById(R.id.textView1)
        val textView2: TextView = itemView.findViewById(R.id.textView2)
    }
}
