package name.kate_kniazeva.my_list

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    lateinit var adapter: CustomAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment1_layout)
        val recyclerview = findViewById<RecyclerView>(R.id.recyclerview)
        recyclerview.layoutManager = LinearLayoutManager(this)

        val data = ArrayList<ItemsViewModel>()
        for (i in 1..1000) {
            data.add(
                ItemsViewModel(
                    R.drawable.baseline_task_alt_black_20,
                    "Title " + i,
                    "Description " + i
                )
            )
        }


        val adapter = CustomAdapter(data)
        recyclerview.adapter = adapter


    }
}


